﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geodata
{
    class Setup
    {
        private static Setup _Instance;
        public String ConnectionString;
       
        private Setup()      // Constructor
        {
            //ConnectionString = @"Data Source=localhost\SQL2008;Initial Catalog=_ptSQL_SpielwieseXXL;Integrated Security=True";            
            //ReadConfiguration("Config")
        }

       public void InitFromCommandline(string[] args)
        {
            try
            {

                ConnectionString = args[0];
                ConnectionString = ConnectionString.Replace("mablepassword", "sme_mwo_mre2win");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Nicht genügend Argumente angeben. " + ex.Message);
            }
           
        }

        public static Setup Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new Setup();
                }
                return _Instance;
            }
        }

    }
}
