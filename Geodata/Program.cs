﻿using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.IO.Compression;
using System.Data.SqlClient;

namespace Geodata
{
    class Program
    {
        static void Main(string[] args)
        {
            Setup.Instance.InitFromCommandline(args);
            CustomersList Liste = new CustomersList();
            Liste.Run();

        }
    }
}
