﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Geodata
{
    class CustomerData
    {
        DataRow Data;
        double lat;
        double lon;

        public CustomerData(DataRow row)
        {
            this.Data = row;
        }
        public void GetLonLat()
        {
            string url = "https://nominatim.openstreetmap.org/search?street=" + Data["sAddress"].ToString() + "&city=" + Data["sCity"].ToString() + "&country=" + Data["sCountry"].ToString() + "&postalcode=" + Data["sPostalCode"].ToString() + "&format=json&json_callback =<string>";

            var request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.AllowAutoRedirect = true;
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17";

            var response = (HttpWebResponse)request.GetResponse();
            var streamReader = new StreamReader(response.GetResponseStream());
            string content = streamReader.ReadToEnd();
            dynamic stuff = JsonConvert.DeserializeObject(content);
            List<double> GeoData = new List<double>();

            try
            {
                double x1 = stuff[0].boundingbox[0];
                double x2 = stuff[0].boundingbox[1];
                double y1 = stuff[0].boundingbox[2];
                double y2 = stuff[0].boundingbox[3];
                double latzahl = (x1 + x2) / 2;
                double lonzahl = (y1 + y2) / 2;

                lat = latzahl;
                lon = lonzahl;
                //GeoData.Add(lat);
                //GeoData.Add(lon);
            }
            catch (Exception e)
            {
                Console.WriteLine("Fehler!" + e.Message);
                Console.WriteLine(Data["idCustomerID"]);
            }
        }
        public double GetLat()
        {
            return this.lat;
        }
        public double GetLon()
        {
            return this.lon;
        }
    }
}
