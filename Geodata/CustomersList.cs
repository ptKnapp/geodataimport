﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geodata;
using System.Data.SqlClient;



namespace Geodata
{

    class CustomersList
    {

        

        public void Run()
        {
            int anzahl = 0;
                while((anzahl = getRowCount()) > 0)
                {
                    Console.WriteLine("Es sind noch " + anzahl + " Datensätze vorhanden");
                    GetData();
                }

        }

        private int getRowCount()
        {
            int anzahl = 1;
            try
            {
                using (OleDbConnection conn = new OleDbConnection(Setup.Instance.ConnectionString))
                {
                    string qry = @"Select Count (*) as anzahl From maCustomers where fGeolocation_long is null";
                    OleDbCommand command = new OleDbCommand(qry, conn);
                    conn.Open();
                    OleDbDataReader reader = command.ExecuteReader();
                    reader.Read();
                    anzahl = Convert.ToInt32(reader["anzahl"]);
                    reader.Close();
                }
            }
            catch
            {
            }
            return anzahl;

        }

        private void GetData()
        {
            using (OleDbConnection conn = new OleDbConnection(Setup.Instance.ConnectionString))
            {
                DataTable List = new DataTable();

                try
                {
                    // Vorbereitung der Datenabfrage 
                    string qry = @"select TOP 1 * from maCustomers where fGeolocation_long is null";
                    OleDbDataAdapter da = new OleDbDataAdapter();
                    da.SelectCommand = new OleDbCommand(qry, conn);
                    OleDbCommandBuilder cb = new OleDbCommandBuilder(da);

                    // Füllen der Daten
                    da.Fill(List);
                    foreach (DataRow row in List.Rows)
                    {
                        CustomerData Data = new CustomerData(row);
                        Data.GetLonLat();
                        row["fGeolocation_lat"] = Data.GetLat();
                        row["fGeolocation_long"] = Data.GetLon();
                        da.Update(List);
                        System.Threading.Thread.Sleep(5000);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("FEHLER: " + e.Message);
                    
                }
               

            }
        }
       


    }
}
